
import defaultImg from "../images/default.png";


const emp = [
    {
        name: "Rahool M Wadam",
        email: "rahool.wadam@hurix.com",
        img: defaultImg,
        hobbies: ["Carrom", "8 Ball Pool"]
    }
];

export default emp;